<?php
/*
 * This file is part of the Diamant Routing package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Routing\Tests;

use Diamant\Component\Routing\Route;

abstract class DataGeneratorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Delegate dataGenerator selection to child test classes
     */
    abstract protected function getDataGeneratorClass();

    public function testAddStaticRouteMethod()
    {
        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $dataGenerator->addRoute('GET', ['/foo'], 'handler0');
        $this->assertEquals(
            [
                [
                    'GET' =>
                    [
                        '/foo' => 'handler0'
                    ]
                ],
                []
            ],
            $dataGenerator->getData()
        );
    }

    public function testAddVariableRouteMethod()
    {

        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $dataGenerator->addRoute('GET', ['/foo/', ['bar', '[^/]+']], 'handler0');
        $this->assertAttributeEquals(
            [
                'GET' =>
                [
                    '/foo/([^/]+)' => new Route('GET', 'handler0', '/foo/([^/]+)', ['bar' => 'bar'])
                ]
            ],
            'methodToRegexToRoutesMap',
            $dataGenerator
        );
    }

    public function testDuplicateVariableRouteError()
    {
        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $this->setExpectedException('LogicException', 'Cannot register two routes matching "/foo/([^/]+)" for method "GET"');
        $dataGenerator->addRoute('GET', ['/foo/', ['bar', '[^/]+']], 'handler0');
        $dataGenerator->addRoute('GET', ['/foo/', ['foo', '[^/]+']], 'handler1');
    }

    public function testDuplicateStaticRouteError()
    {
        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $this->setExpectedException('LogicException', 'Cannot register two routes matching "/foo" for method "GET"');
        $dataGenerator->addRoute('GET', ['/foo'], 'handler0');
        $dataGenerator->addRoute('GET', ['/foo'], 'handler1');
    }

    public function testDuplicateVariableNameError()
    {
        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $this->setExpectedException('LogicException', 'Cannot use the same placeholder "bar" twice');
        $dataGenerator->addRoute('GET', ['/foo/',  ['bar', '[^/]+'],  ['bar', '\d+']], 'handler0');
    }

    public function testShadowedStaticRouteError()
    {
        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $this->setExpectedException('LogicException', 'Static route "/foo/bar" is shadowed by previously defined variable route "/foo/([^/]+)" for method "GET"');
        $dataGenerator->addRoute('GET', ['/foo/',  ['bar', '[^/]+']], 'handler0');
        $dataGenerator->addRoute('GET', ['/foo/bar'], 'handler1');
    }

    public function testCapturingError()
    {
        $className = $this->getDataGeneratorClass();
        $dataGenerator = new $className();
        $this->setExpectedException('LogicException', 'Regex "(bar|foo)" for parameter "foo" contains a capturing group');
        $dataGenerator->addRoute('GET', ['/',  ['foo', '(bar|foo)']], 'handler0');
    }
}
