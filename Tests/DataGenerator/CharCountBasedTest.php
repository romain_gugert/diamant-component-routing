<?php
/*
 * This file is part of the Diamant Routing package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Routing\Tests\DataGenerator;

use Diamant\Component\Routing\Tests\DataGeneratorTest;

class CharCountBasedTest extends DataGeneratorTest
{
    protected function getDataGeneratorClass() {
        return 'Diamant\\Component\\Routing\\DataGenerator\\CharCountBased';
    }
}
