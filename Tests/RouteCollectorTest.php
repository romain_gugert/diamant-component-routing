<?php
/*
 * This file is part of the Diamant Routing package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Routing\Tests;

use Diamant\Component\Routing\RouteCollector;
use Diamant\Component\Routing\DataGenerator\GroupCountBased;
use Diamant\Component\Routing\RouteParser;

class RouteCollectorTest extends \PhpUnit_Framework_TestCase
{

    public function testConstructorWithCustomArgs()
    {
        $routeCollector = new RouteCollector(new RouteParser(), new GroupCountBased());

        $this->assertAttributeEquals(new RouteParser(), 'routeParser', $routeCollector);
        $this->assertAttributeEquals(new GroupCountBased(), 'dataGenerator', $routeCollector);
    }

    public function testMethodAddRoute()
    {
        $routeCollector = new RouteCollector(new RouteParser(), new GroupCountBased());
        $parser = new RouteParser();
        $dataGenerator = new GroupCountBased();


        $routeCollector->addRoute(['GET', 'POST'], '/foo/{bar}/{id:[0-9]+}', 'handler0');
        $dataGenerator->addRoute('GET', $parser->parse('/foo/{bar}/{id:[0-9]+}')[0], 'handler0');
        $dataGenerator->addRoute('POST', $parser->parse('/foo/{bar}/{id:[0-9]+}')[0], 'handler0');

        $testRouteCollector = new RouteCollector($parser, $dataGenerator);
        $this->assertEquals($routeCollector->getData(), $testRouteCollector->getData());
    }

}
