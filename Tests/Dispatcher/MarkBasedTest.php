<?php
/*
 * This file is part of the Diamant Routing package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Routing\Tests\Dispatcher;

use Diamant\Component\Routing\Tests\DispatcherTest;

class MarkBasedTest extends DispatcherTest
{
    public function setUp()
    {
        preg_match('/(*MARK:A)a/', 'a', $matches);
        if (!isset($matches['MARK'])) {
            $this->markTestSkipped('PHP 5.6 required for MARK support');
        }
    }

    protected function getDispatcherClass()
    {
        return 'Diamant\\Component\\Routing\\Dispatcher\\MarkBased';
    }

    protected function getDataGeneratorClass()
    {
        return 'Diamant\\Component\\Routing\\DataGenerator\\MarkBased';
    }
}
