<?php
/*
 * This file is part of the Diamant Routing package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Routing\Tests;

use \ReflectionProperty;
use Diamant\Component\Routing\Route;

class RoutingTest extends \PHPUnit_Framework_TestCase
{

    public function testConstructorWithCustomArgs()
    {
        $route = new Route('GET', 'handler0', '/foo/{bar}');

        $this->assertAttributeEquals('GET', 'method', $route);
        $this->assertAttributeEquals('handler0', 'handler', $route);
        $this->assertAttributeEquals('/foo/{bar}', 'regex', $route);
        $this->assertAttributeEquals([], 'variables', $route);
    }


    public function testMatchesMethod()
    {
        $route = new Route('GET', 'handler0', '/foo/\d+');

        $this->assertTrue($route->matches('/foo/2596'));
        $this->assertFalse($route->matches('/foo/bar'));
    }

}
