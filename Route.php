<?php
/*
 * This file is part of the Diamant Routing package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Routing;

class Route
{
    /**
     * The http method
     *
     * @var string
     */
    protected $method;

    /**
     * The regex pattern
     *
     * @var string
     */
    protected $regex;

    /**
     * The variables
     *
     * @var array
     */
    protected $variables = [];

    /**
     * The handler
     *
     * @var mixed
     */
    protected $handler;

    /**
     * Constructs a route (value object).
     *
     * @param string $method
     * @param mixed  $handler
     * @param string $regex
     * @param array  $variables
     */
    public function __construct($method, $handler, $regex, array $variables = [])
    {
        $this->method = $method;
        $this->handler = $handler;
        $this->regex = $regex;
        $this->variables = $variables;
    }

    /**
     * Regex Getter
     *
     * @return string
     */
    public function getRegex()
    {
        return $this->regex;
    }

    /**
     * Method Getter
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Handler Getter
     *
     * @return string
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * Variables Getter
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * Tests whether this route matches the given string.
     *
     * @param string $str
     * @return bool
     */
    public function matches($str)
    {
        $regex = '~^' . $this->regex . '$~';
        return (bool) preg_match($regex, $str);
    }
}

