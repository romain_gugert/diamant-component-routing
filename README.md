Diamant Routing - Fast request router for PHP
=======================================

This library is [nikic](https://github.com/nikic)/[fast-route](https://github.com/nikic/FastRoute/) fork.

Install
-------

To install with composer:

```sh
composer require diamant/routing
```

Usage
-----

Here's a basic usage example:
```php
<?php

require '/path/to/vendor/autoload.php';

$dispatcher = Diamant\Component\Routing\simpleDispatcher(function(Diamant\Component\Routing\RouteCollector $r) {
    $r->addRoute('GET', '/users', 'get_all_users_handler');
    // {id} must be a number (\d+)
    $r->addRoute('GET', '/user/{id:\d+}', 'get_user_handler');
    // The /{title} suffix is optional
    $r->addRoute('GET', '/articles/{id:\d+}[/{title}]', 'get_article_handler');
});

$routeCollector = new Diamant\Component\Routing\RouteCollector(
    new Diamant\Component\Routing\RouteParser(), new Diamant\Component\Routing\DataGenerator\GroupCountBased()
);

$routeCollector->addRoute('GET', '/users', 'get_all_users_handler');
// {id} must be a number (\d+)
$routeCollector->addRoute('GET', '/user/{id:\d+}', 'get_user_handler');
// The /{title} suffix is optional
$routeCollector->addRoute('GET', '/articles/{id:\d+}[/{title}]', 'get_article_handler');

$dispatcher = new Diamant\Component\Routing\Dispatcher\GroupCountBased($routeCollector);

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = rawurldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case Diamant\Component\Routing\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case Diamant\Component\Routing\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case Diamant\Component\Routing\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        // ... call $handler with $vars
        break;
}
```
